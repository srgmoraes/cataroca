require 'test_helper'
require 'i18n'

class ProjetoTest < ActiveSupport::TestCase

  setup do
     @projeto_a = Projeto.new(:nome => "projeto_a")
     @projeto_b = Projeto.new(:nome => "projeto_b")
  end

  test "Salvar projeto" do
    assert @projeto_a.save
  end

  test "Nao salvar projeto com nome que ja existe" do
    assert @projeto_a.save

    projeto_nome_duplicado = Projeto.new(:nome => "projeto_a")
    assert !projeto_nome_duplicado.save()
    assert_equal projeto_nome_duplicado.errors.size, 1
    assert_not_nil projeto_nome_duplicado.errors.get(:nome)
    assert_equal projeto_nome_duplicado.errors.get(:nome), [I18n.t('.projeto.nome.unico')]
  end

  test "Atualizar projeto" do
    assert @projeto_a.save

    @projeto_a = Projeto.find_by_nome("projeto_a")
    @projeto_a.nome = "projeto_ab"

    assert @projeto_a.save
    assert Projeto.find_by_nome("projeto_ab")
    assert_nil Projeto.find_by_nome("projeto_a")
  end

  test "Nao atualizar projeto para nome vazio" do
    assert @projeto_a.save

    @projeto_a = Projeto.find_by_nome("projeto_a")
    @projeto_a.nome = ""
    assert !@projeto_a.save
    assert_not_nil @projeto_a.errors.empty?
    assert_equal @projeto_a.errors.size, 1
    assert_not_nil @projeto_a.errors.get(:nome)
    assert_equal @projeto_a.errors.get(:nome), [I18n.t('.projeto.nome.invalido')]
  end

  test "Nao atualizar projeto com nome que ja existe" do
    assert @projeto_a.save
    assert @projeto_b.save

    @projeto_b.nome = "projeto_a"
    assert !@projeto_b.save

    assert_equal  @projeto_a.nome, @projeto_b.nome
    assert_not_nil @projeto_b.errors.empty?
    assert_equal @projeto_b.errors.size, 1
    assert_not_nil @projeto_b.errors.get(:nome)
    assert_equal @projeto_b.errors.get(:nome), [I18n.t('.projeto.nome.unico')]
  end

  test "Nao salvar projeto para nome vazio" do
    @projeto_a.nome = ""
    assert !@projeto_a.save

    assert_not_nil @projeto_a.errors.empty?
    assert_equal @projeto_a.errors.size, 1
    assert_not_nil @projeto_a.errors.get(:nome)
    assert_equal @projeto_a.errors.get(:nome), [I18n.t('.projeto.nome.invalido')]
  end

  test "Deletar logicamente" do
    assert @projeto_a.save
    assert_equal @projeto_a.ativo, 1
    assert @projeto_a.destroy
    assert_equal @projeto_a.ativo, 0
  end

end
