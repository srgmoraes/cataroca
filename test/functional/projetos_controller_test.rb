require 'test_helper'

class ProjetosControllerTest < ActionController::TestCase

  setup do
    @projeto = projetos(:tarantino)
  end

  test "Acessar Projetos" do
    get :index
    assert_response :success
    assert_not_nil assigns(:projetos)
  end

  test "Criar Projeto" do
    get :new
    assert_response :success
  end

  test "Salvar Projeto" do

    #post :create, :projeto => @projeto
    projeto = {:nome => "projeto_a"}
    post :create, :projeto => projeto

    assert_redirected_to projeto_path(assigns(:projeto))
    assert_equal assigns(:projeto).nome, projeto[:nome]
    assert_equal flash[:notice], I18n.t('.projeto.criado')

  end

  test "Salvar Projeto nome em branco" do

    #post :create, :projeto => @projeto
    projeto = {:nome => ""}
    post :create, :projeto => projeto

    assert_response :success
    assert_equal assigns(:projeto).errors.get(:nome), [I18n.t('.projeto.nome.invalido')]

  end

  test "Salvar Projeto nome que ja existe" do

    #post :create, :projeto => @projeto
    projeto = {:nome => "tarantino"}
    post :create, :projeto => projeto

    assert_response :success
    assert_equal assigns(:projeto).errors.get(:nome), [I18n.t('projeto.nome.unico')]

  end

  test "Editar Projeto" do

    get :edit, :id => @projeto.to_param

    assert_response :success
    assert_not_nil assigns(:projeto).nome
    assert_equal assigns(:projeto).nome, @projeto.nome

  end

  test "Atualizar Projeto" do

    put :update, :id => @projeto.to_param, :projeto => @projeto.attributes

    assert_redirected_to projeto_path(assigns(:projeto))
    assert_equal flash[:notice], I18n.t('.projeto.atualizado')

  end

  test "Deletar Projeto" do

    assert_difference('Projeto.find_all_by_ativo(1).size', -1) do
      delete :destroy, :id => @projeto.to_param
    end

    assert_redirected_to projetos_path

  end

end
