class CreateBd < ActiveRecord::Migration
  def change

    create_table :projetos do |t|
      t.string :nome
      t.integer :ativo, :default => 1,  :null => false

      t.timestamps
    end

    create_table :sprints do |t|
      t.string :nome,  :null => false
      t.string :descricao
      t.date :inicio,  :null => false
      t.date :fim,  :null => false
      t.integer :ativo, :default => 1,  :null => false
      t.references :projeto,  :null => false

      t.timestamps
    end

  end
end
