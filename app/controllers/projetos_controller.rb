class ProjetosController < ApplicationController

  def index
    @projetos = Projeto.all
  end

  def show
    @projeto = Projeto.find(params[:id])
  end

  def new
    @projeto = Projeto.new
  end

  def edit
    @projeto = Projeto.find(params[:id])
  end

  def create
    @projeto = Projeto.new(params[:projeto])

    if @projeto.save
      redirect_to @projeto, :notice => I18n.t('.projeto.criado')
    else
      render :action => "new"
    end
  end

  def update
    @projeto = Projeto.find(params[:id])

    if @projeto.update_attributes(params[:projeto])
      redirect_to @projeto, :notice => I18n.t('.projeto.atualizado')
    else
      render :action => "edit"
    end
  end

  def destroy
    @projeto = Projeto.find(params[:id])
    @projeto.destroy

    redirect_to projetos_url
  end
end
