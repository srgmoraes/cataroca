class Sprint < ActiveRecord::Base

  before_update :validar, :validar_atualizar
  before_create :validar, :validar_criar

  belongs_to :projeto

  def validar

    if self.nome.blank?
      self.errors.add(:nome, I18n.t('.sprint.nome.invalido'))
    end

    if self.inicio.blank?
      self.errors.add(:inicio, I18n.t('.sprint.inicio.invalido'))
    end

    if self.fim.blank?
      self.errors.add(:fim, I18n.t('.sprint.fim.invalido'))
    end

    if !self.fim.blank? and !self.fim.blank? and self.fim < self.inicio
      self.errors.add(:fim, I18n.t('.sprint.fim.menor'))
    end

    if self.projeto.blank?
      self.errors.add(:projeto, I18n.t('.projeto.projeto.invalido'))
    end

    self.errors.empty?

  end

  def validar_criar



    self.errors.empty?

  end

  def validar_atualizar



    self.errors.empty?

  end

end
