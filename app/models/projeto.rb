class Projeto < ActiveRecord::Base

  before_update :validar, :validar_atualizar
  before_create :validar, :validar_criar

  has_many :sprints

  def validar

    if self.nome.blank?
      self.errors.add(:nome, I18n.t('.projeto.nome.invalido'))
    end

    self.errors.empty?

  end

  def validar_criar

    unless Projeto.where("nome = :nome and ativo = 1", {:nome => self.nome}).empty?
      self.errors.add(:nome, I18n.t('.projeto.nome.unico'))
    end

    self.errors.empty?

  end

  def validar_atualizar

    unless Projeto.where("nome = :nome and id != :id and ativo = 1", {:nome => self.nome, :id => self.id}).empty?
      self.errors.add(:nome, I18n.t('.projeto.nome.unico'))
    end

    self.errors.empty?

  end

  def destroy
    self.update_column(:ativo, 0)
  end

end
